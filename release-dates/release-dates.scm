(use-modules (fibers)
             (fibers channels)
             (web client)
             (web response)
             (web uri)
             (srfi srfi-1)
             (srfi srfi-19)
             (ice-9 match)
             (ice-9 textual-ports))

(define (lines file)
  (let ((out (make-channel)))
    (spawn-fiber
     (lambda ()
       (call-with-input-file file
         (lambda (p)
           (let lp ()
             (let ((line (get-line p)))
               (cond
                ((eof-object? line)
                 (put-message out #f))
                (else
                 (put-message out (substring line 1))))
               (lp)))))))
    out))

(define (filter-tarballs in)
  (define (is-tarball? line)
    (or-map (lambda (suffix) (string-suffix-ci? suffix line))
            '(".tar.gz" ".tar.bz2" ".tar.lz" ".tar.xz" ".zip")))
  (let ((out (make-channel)))
    (spawn-fiber
     (lambda ()
       (let lp ()
         (cond
          ((get-message in)
           => (lambda (line)
                (when (and (string-prefix? "/gnu/" line)
                           (is-tarball? line))
                  (put-message out line))))
          (else
           (put-message out #f)))
         (lp))))
    out))

(define *addresses* (make-hash-table))
(define (lookup-addresses host port)
  (or (hash-ref *addresses* (cons host port))
      (let ((ret (delete-duplicates
                  (getaddrinfo host (number->string port) AI_NUMERICSERV)
                  (lambda (ai1 ai2)
                    (equal? (addrinfo:addr ai1) (addrinfo:addr ai2))))))
        (hash-set! *addresses* (cons host port) ret)
        ret)))

(define (open-socket-for-uri uri)
  "Return an open input/output port for a connection to URI."
  (let loop ((addresses (lookup-addresses (uri-host uri) (uri-port uri))))
    (let* ((ai (car addresses))
           (s  (with-fluids ((%default-port-encoding #f))
                 ;; Restrict ourselves to TCP.
                 (socket (addrinfo:fam ai) SOCK_STREAM IPPROTO_IP))))
      (catch 'system-error
             (lambda ()
               (connect s (addrinfo:addr ai))
               (setvbuf s 'block 4000)
               (let ((flags (fcntl s F_GETFL)))
                 (fcntl s F_SETFL (logior O_NONBLOCK flags)))
               s)
             (lambda args
               ;; Connection failed, so try one of the other addresses.
               (close s)
               (if (null? (cdr addresses))
                   (apply throw args)
                   (loop (cdr addresses))))))))

(define (get-date path)
  (let ((uri (build-uri 'http #:host "ftp.gnu.org" #:port 80 #:path path)))
    (response-last-modified
     (http-head uri #:port (open-socket-for-uri uri)))))

(define (get-dates paths out)
  (let lp ()
    (match (get-message paths)
      (#f
       (put-message out #f))
      (path
       (put-message out (cons (get-date path) path))
       (lp)))))

(define* (parallelize-i/o in f #:key (par 100)
                          (tombstone? (lambda (x) (not x))))
  (let ((join (make-channel))
        (out (make-channel)))
    (let lp ((n 0))
      (when (< n par)
        (spawn-fiber (lambda () (f in join)))
        (lp (1+ n))))
    (spawn-fiber
     (lambda ()
       (let lp ((pending par))
         (cond
          ((zero? pending)
           (put-message out #f))
          (else
           (match (get-message join)
             ((? tombstone?)
              (lp (1- pending)))
             (msg
              (put-message out msg)
              (lp pending))))))))
    out))

(define (resolve-dates file)
  (let ((dates (parallelize-i/o (filter-tarballs (lines file))
                                get-dates)))
    (let lp ((count 0))
      (match (get-message dates)
        (#f 'done)
        ((date . path)
         (when date
           (format #t "~a,~a\n" (date->string date "~Y-~m-~d ~H:~M:~S~z") path))
         (lp (1+ count)))))))

(match (command-line)
  ((_ find.txt)
   (run-fibers
    (lambda ()
      (resolve-dates find.txt))))
  (_
   (format (current-error-port) "Usage: guile release-dates.scm find.txt\n")
   (exit 1)))
