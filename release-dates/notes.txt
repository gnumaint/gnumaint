releases indicate active packages

active project count by year

active: release <= 3yo

releases are:
  - ftp.gnu.org/ftp/foo/foo-1.2.3.tar.gz

  - manually collected release list for the ~90 GNU packages not on
    gnu.org

  - excluding decomissioned packages (see 

  - excluding GNOME, gimp, glib, gnat, gnowsys, gnumeric, gnustep, r

  - problems with smaller packages: java-getopt, kopi, melting, nana, pies, polyxmass, stump; webstump

  - lilypond only date of last major release

  - excluding alpha.gnu.org

  - sysutils??

  - excluding talkfilters because it's disgusting

  - teximpatient is a book?

  - issues getting texmacs release series; try again from git tags

  - a number of packages only with releases on gna.org, wayback machine
    is the only thing

  - cfengine 404

  - could not find older djgpp releases

  - do these packages consider themselves to be gnu?

  - eprints dates not available
