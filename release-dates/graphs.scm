(use-modules (ice-9 match)
             (ice-9 textual-ports)
             (charting))

(define* (release-counts file)
  (call-with-input-file file
    (lambda (p)
      (let lp ()
        (let ((line (get-line p)))
          (if (eof-object? line)
              '()
              (match (string-split line #\,)
                ;; FIXME: read versioned data
                ((date . counts)
                 (acons date
                        (map (lambda (x)
                               (or (string->number x)
                                   (error "unexpected" x)))
                             counts)
                        (lp))))))))))

(make-bar-chart
 "GNU Project Health: First and Last Releases"
 (map (lambda (x)
        (match x
          ((date starts ends)
           `(,date (,starts "first") (,ends "last")))))
      (release-counts "terminal-releases-by-year.txt"))
 #:y-axis-label "number of projects"
 #:bar-width 10
 #:group-spacing 10
 #:ytick-label-formatter
 (lambda (y) (number->string (inexact->exact (round y))))
 #:vertical-xtick-labels? #t
 #:write-to-png "terminal-releases-by-year.png")

(make-bar-chart
 "GNU Project Health: Active Project Count (release ≤ 3 years ago)"
 (map (lambda (x)
        (match x
          ((date count)
           `(,date (,count "active projects")))))
      (release-counts "active-projects-by-year.txt"))
 #:bar-width 10
 #:group-spacing 10
 #:ytick-label-formatter
 (lambda (y) (number->string (inexact->exact (round y))))
 #:vertical-xtick-labels? #t
 #:write-to-png "active-projects-by-year.png")
