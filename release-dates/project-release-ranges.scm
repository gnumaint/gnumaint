(use-modules (srfi srfi-1)
             (srfi srfi-9)
             (srfi srfi-19)
             (ice-9 match)
             (ice-9 textual-ports))

(define (date-before? d1 d2)
  (time<? (date->time-tai d1) (date->time-tai d2)))

(define* (release-dates file #:key (select (lambda (package date) #t)))
  (call-with-input-file file
    (lambda (p)
      (let lp ()
        (let ((line (get-line p)))
          (if (eof-object? line)
              '()
              (match (string-split line #\,)
                ;; FIXME: read versioned data
                ((date package . _)
                 (let ((date (string->date date "~Y-~m-~d ~H:~M:~S~z")))
                   (if (select package date)
                       (acons date package (lp))
                       (lp)))))))))))

(define (group-by ls less? truncate zero add)
  (let lp ((ls (sort ls less?))
           (cur #f)
           (sum (zero)))
    (define (finish tail)
      (if cur
          (acons cur sum tail)
          tail))
    (match ls
      (() (finish '()))
      ((x . ls)
       (let ((x_ (truncate x)))
         (if (equal? x_ cur)
             (lp ls cur (add x sum))
             (finish (lp ls x_ (add x (zero))))))))))

(define (calculate-release-ranges releases)
  (group-by releases
            (lambda (a b)
              (match (vector a b)
                (#((a-date . project) (b-date . project))
                 (date-before? a-date b-date))
                (#((_ . a-project) (_ . b-project))
                 (string<=? a-project b-project))))
            cdr
            (lambda () #f)
            (lambda (new old)
              (match new
                ((date . project)
                 (match old
                   (#f (vector date date))
                   (#(start end)
                    (vector start date))))))))

(match (command-line)
  ((_ releases.txt)
   (for-each (match-lambda
              ((project . #(start end))
               (format #t "~a,~a,~a\n"
                       project
                       (date->string start "~Y-~m-~d ~H:~M:~S~z")
                       (date->string end "~Y-~m-~d ~H:~M:~S~z"))))
             (calculate-release-ranges (release-dates releases.txt))))
  (_
   (format (current-error-port)
           "Usage: guile project-release-ranges.scm releases.txt\n")
   (exit 1)))
