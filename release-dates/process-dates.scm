(use-modules (srfi srfi-19)
             (ice-9 match)
             (ice-9 textual-ports)
             (ice-9 regex))

(define *all-releases* (make-hash-table))

(define (date-tai-difference d1 d2)
  (let ((diff (time-difference (date->time-tai d1) (date->time-tai d2))))
    (+ (time-second diff) (* 1e-9 (time-nanosecond diff)))))

(define (date-before? d1 d2)
  (negative? (date-tai-difference d1 d2)))

(define (absolute-time-difference-more-than? d1 d2 seconds)
  (< seconds (abs (date-tai-difference d1 d2))))

(define (more-than-a-month-apart? d1 d2)
  (absolute-time-difference-more-than? d1 d2 (* 30 24 60 60)))

(define (add-artifact! project version head date)
  (let* ((key (vector project version head)))
    (match (hash-ref *all-releases* key)
      (#f (hash-set! *all-releases* key date))
      (existing
       (when (more-than-a-month-apart? date existing)
         (format (current-error-port)
                 "Warning: Project ~a has artifacts for ~a:~a with timestamps more than 30 days apart:\n"
                 project version head)
         (format (current-error-port) "  ~a versus ~a\n\n"
                 (date->string existing "~Y-~m-~d ~H:~M:~S~z")
                 (date->string date "~Y-~m-~d ~H:~M:~S~z"))
         (when (date-before? date existing)
           ;; Keep the earlier date.
           (hash-set! *all-releases* key date)))))))

(define (strip-suffixes path suffixes)
  (let lp ((path path)
           (suffixes suffixes))
    (match suffixes
      (() path)
      ((suffix . suffixes)
       (lp (if (string-suffix-ci? suffix path)
               (substring path 0 (- (string-length path) (string-length suffix)))
               path)
           suffixes)))))

(define (strip-tarball-suffixes path)
  (strip-suffixes path '(".tar.gz" ".tar.bz2" ".tar.lz" ".tar.xz" ".zip")))
(define (strip-binary-suffixes path)
  (define suffixes
    '("linux" "sources" "no-deps" "x86-64" "i386" "i686" "x86_64"
      "gnulinux" "for-windows" "barebin" "bin" "w32" "en-US" "gnu1"
      "svm1" "doc" "html" "pdf" "info" "w32" "en_US" "win32"
      "aarch64" "armhf" "gnu" "devel" "src" "mingw32" "w64" "w64-64"
      "pkg" "win" "pack" "deps" "mingw" "win64" "gnu2" "msw" "pc"
      "orig" "linux-32" "64bit" "x64" "kfw322" "x86" "ucode" "ix86"
      "apple" "darwin" "powerpc" "sparc" "sun" "solaris" "openbsd"
      "universal" "solaris2.9" "mac" "linux386" "generics" "tv" "bin-r1"
      "vc++" "nocurses" "woe32" "ps" "os2" "freebsd-4.3-RELEASE"
      "unknown" "sunos-5.8" "unknown-linux-2.4.18-10"
      "cygwin32-1.3.12" "cygwin32_nt-5.0-1.3.12" "source"))
  (define separators (string->char-set "_-."))
  (define (strip-a-suffix path)
    (or-map (lambda (suffix)
              (let ((plen (string-length path))
                    (slen (string-length suffix)))
                (and (> plen slen)
                     (string-suffix? suffix path)
                     (char-set-contains? separators
                                         (string-ref path (- plen slen 1)))
                     (substring path 0 (- plen slen 1)))))
            suffixes))
  (define (fixpoint f arg)
    (match (f arg)
      (#f arg)
      (arg (fixpoint f arg))))
  (fixpoint strip-a-suffix path))

(define version-re (make-regexp "^(.*)[_-]([0-9].*)$"))
(define (visit-release-date date path)
  (match (string-split path #\/)
    (("" "gnu" project _ ... basename)
     (let ((basename* (strip-binary-suffixes
                       (strip-tarball-suffixes basename))))
       (match (regexp-exec version-re basename*)
         (#f
          (unless (string-suffix? "-latest" basename*)
            (format (current-error-port)
                    "Warning: Don't know how to parse version from ~a artifact `~a'\n"
                    project basename))
          #f)
         (m (match (list (match:substring m 1) (match:substring m 2))
              ((head version)
               (unless (or (string-contains version "latest")
                           (string-contains basename "debian"))
                 (add-artifact! project version head date))))))))
    (_
     (format (current-error-port) "Warning: Unexpected path: `~a'\n" path))))

(define (for-each-release-date file)
  (call-with-input-file file
    (lambda (p)
      (let lp ()
        (let ((line (get-line p)))
          (unless (eof-object? line)
            (match (string-split line #\,)
              ((date path)
               (visit-release-date (string->date date "~Y-~m-~d ~H:~M:~S~z") path)
               (lp)))))))))

(define (for-each-manually-collected-release-date file)
  (call-with-input-file file
    (lambda (p)
      (let lp ()
        (let ((line (get-line p)))
          (unless (eof-object? line)
            (match (string-split line #\,)
              ((date project version)
               (let* ((date (string->date date "~Y-~m-~d"))
                      (utc-date (make-date 0 0 0 0
                                           (date-day date)
                                           (date-month date)
                                           (date-year date)
                                           0)))
                 (add-artifact! project version project utc-date))
               (lp)))))))))

(define (delete-duplicates-within-a-month project version date releases)
  (match releases
    (() '())
    (((_ . (? (lambda (d) (more-than-a-month-apart? d date)))) . _)
     releases)
    ((release . releases)
     (let ((releases (delete-duplicates-within-a-month project version date releases)))
       (match release
         ((#(project* version* _) . _)
          (if (and (equal? project project*) (equal? version version*))
              releases
              (cons release releases))))))))

(define (prune-duplicates releases)
  (match releases
    (() '())
    ((release . releases)
     (match release
       ((#(project version head) . date)
        (cons release
              (prune-duplicates
               (delete-duplicates-within-a-month project version date
                                                 releases))))))))

(define (summarize-releases)
  (for-each
   (match-lambda
    ((#(project version head) . date)
     (format #t "~a,~a,~a,~a\n"
             (date->string date "~Y-~m-~d ~H:~M:~S~z") project head version)))
   (prune-duplicates
    (sort (hash-map->list cons *all-releases*)
          (match-lambda*
           (((k1 . d1) (k2 . d2)) (date-before? d1 d2)))))))

(match (command-line)
  ((_ release-dates.txt manual-release-dates.txt)
   (for-each-release-date release-dates.txt)
   (for-each-manually-collected-release-date manual-release-dates.txt)
   (summarize-releases))
  (_
   (format (current-error-port)
           "Usage: guile process-dates.scm release-dates.txt manual-release-dates.txt\n")
   (exit 1)))
