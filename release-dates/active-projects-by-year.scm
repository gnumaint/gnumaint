(use-modules (srfi srfi-1)
             (srfi srfi-9)
             (srfi srfi-19)
             (ice-9 match)
             (ice-9 textual-ports))

(define (date-before? d1 d2)
  (time<? (date->time-tai d1) (date->time-tai d2)))

(define* (release-dates file #:key (select (lambda (package date) #t)))
  (call-with-input-file file
    (lambda (p)
      (let lp ()
        (let ((line (get-line p)))
          (if (eof-object? line)
              '()
              (match (string-split line #\,)
                ;; FIXME: read versioned data
                ((date package . _)
                 (let ((date (string->date date "~Y-~m-~d ~H:~M:~S~z")))
                   (if (select package date)
                       (acons date package (lp))
                       (lp)))))))))))

(define (keyed-sorter key less?)
  (lambda (a b) (less? (key a) (key b))))

(define (group-by ls less? truncate zero add)
  (let lp ((ls (sort ls less?))
           (cur #f)
           (sum (zero)))
    (define (finish tail)
      (if cur
          (acons cur sum tail)
          tail))
    (match ls
      (() (finish '()))
      ((x . ls)
       (let ((x_ (truncate x)))
         (if (equal? x_ cur)
             (lp ls cur (add x sum))
             (finish (lp ls x_ (add x (zero))))))))))

(define (fill-in-years ls zero)
  (define (next-year date)
    (make-date 0 0 0 0 1 1 (1+ (date-year date)) 0))
  (let lp ((ls ls))
    (match ls
      (() ls)
      ((_) ls)
      (((date . val) . (and ls* ((date* . _) . _)))
       (let ((next (next-year date)))
         (if (equal? next date*)
             ls
             (acons date val
                    (lp (acons next (zero) ls*)))))))))

(define (count-projects-by-year releases)
  (define (truncate-to-year date)
    (make-date 0 0 0 0 1 1 (date-year date) 0))
  (fill-in-years
   (group-by releases
             (keyed-sorter car date-before?)
             (compose truncate-to-year car)
             (lambda () '())
             (lambda (item set)
               (match item
                 ((date . project)
                  (if (member project set)
                      set
                      (cons project set))))))
   (lambda () '())))

(define (compute-active-projects releases count)
  (define (compute-active packages releases count)
    (if (zero? count)
        packages
        (match releases
          (() packages)
          (((_ . packages*) . releases)
           (compute-active (lset-union equal? packages packages*)
                           releases (1- count))))))
  (match releases
    (() '())
    (((date . packages) . releases)
     (acons date (compute-active packages releases count)
            (compute-active-projects releases count)))))

(match (command-line)
  ((_ releases.txt)
   (for-each (match-lambda
              ((date . projects)
               (format #t "~a,~a\n" (date->string date "~Y")
                       (length projects))))
             (reverse
              (compute-active-projects
               (reverse
                (count-projects-by-year (release-dates releases.txt)))
               2))))
  (_
   (format (current-error-port)
           "Usage: guile active-projects-by-year.scm releases.txt\n")
   (exit 1)))
