(use-modules (srfi srfi-1)
             (srfi srfi-9)
             (srfi srfi-19)
             (ice-9 match)
             (ice-9 textual-ports))

(define (date-before? d1 d2)
  (time<? (date->time-tai d1) (date->time-tai d2)))

(define* (release-dates file #:key (select (lambda (package date) #t)))
  (call-with-input-file file
    (lambda (p)
      (let lp ()
        (let ((line (get-line p)))
          (if (eof-object? line)
              '()
              (match (string-split line #\,)
                ;; FIXME: read versioned data
                ((date package . _)
                 (let ((date (string->date date "~Y-~m-~d ~H:~M:~S~z")))
                   (if (select package date)
                       (acons date package (lp))
                       (lp)))))))))))

(define (keyed-sorter key less?)
  (lambda (a b) (less? (key a) (key b))))

(define (group-by ls less? truncate zero add)
  (let lp ((ls (sort ls less?))
           (cur #f)
           (sum (zero)))
    (define (finish tail)
      (if cur
          (acons cur sum tail)
          tail))
    (match ls
      (() (finish '()))
      ((x . ls)
       (let ((x_ (truncate x)))
         (if (equal? x_ cur)
             (lp ls cur (add x sum))
             (finish (lp ls x_ (add x (zero))))))))))

(define (fill-in-months ls zero)
  (define (next-month date)
    (let ((month (date-month date))
          (year (date-year date)))
      (if (= month 12)
          (make-date 0 0 0 0 1 1 (1+ year) 0)
          (make-date 0 0 0 0 1 (1+ month) year 0))))
  (let lp ((ls ls))
    (match ls
      (() ls)
      ((_) ls)
      (((date . val) . (and ls* ((date* . _) . _)))
       (let ((next (next-month date)))
         (if (equal? next date*)
             ls
             (acons date val
                    (lp (acons next (zero) ls*)))))))))

(define (count-projects-by-month releases)
  (define (truncate-to-month date)
    (make-date 0 0 0 0 1 (date-month date) (date-year date) 0))
  (fill-in-months
   (group-by releases
             (keyed-sorter car date-before?)
             (compose truncate-to-month car)
             (lambda () '())
             (lambda (item set)
               (match item
                 ((date . project)
                  (if (member project set)
                      set
                      (cons project set))))))
   (lambda () '())))

(match (command-line)
  ((_ releases.txt)
   (for-each (match-lambda
              ((date . projects)
               (format #t "~a,~a\n" (date->string date "~Y-~m")
                       (length projects))))
             (count-projects-by-month (release-dates releases.txt))))
  (_
   (format (current-error-port)
           "Usage: guile projects-making-releases-by-month.scm releases.txt\n")
   (exit 1)))
