(use-modules (srfi srfi-1)
             (srfi srfi-9)
             (srfi srfi-19)
             (ice-9 match)
             (ice-9 textual-ports))

(define (date-before? d1 d2)
  (time<? (date->time-tai d1) (date->time-tai d2)))

(define* (release-ranges file)
  (call-with-input-file file
    (lambda (p)
      (let lp ((starts '()) (ends '()))
        (let ((line (get-line p)))
          (if (eof-object? line)
              (values starts ends)
              (match (string-split line #\,)
                ((package start end)
                 (lp (acons package (string->date start "~Y-~m-~d ~H:~M:~S~z")
                            starts)
                     (acons package (string->date end "~Y-~m-~d ~H:~M:~S~z")
                            ends))))))))))

(define (keyed-sorter key less?)
  (lambda (a b) (less? (key a) (key b))))

(define (group-by ls less? truncate zero add)
  (let lp ((ls (sort ls less?))
           (cur #f)
           (sum (zero)))
    (define (finish tail)
      (if cur
          (acons cur sum tail)
          tail))
    (match ls
      (() (finish '()))
      ((x . ls)
       (let ((x_ (truncate x)))
         (if (equal? x_ cur)
             (lp ls cur (add x sum))
             (finish (lp ls x_ (add x (zero))))))))))

(define (merge-sorted-lists xs ys less? truncate zero add)
  (let lp ((xs xs)
           (ys ys))
    (define (adjoin x y xs ys)
      (cons (add x y) (lp xs ys)))
    (match (vector xs ys)
      (#(() ())
       '())
      (#((x . xs) ())
       (adjoin x (zero) xs ys)
       '())
      (#(() (y . ys))
       (adjoin (zero) y xs ys))
      (#((x . xs*) (y . ys*))
       (let ((x_ (truncate x))
             (y_ (truncate y)))
         (cond
          ((less? x_ y_)
           (adjoin x (zero) xs* ys))
          ((less? y_ x_)
           (adjoin (zero) y xs ys*))
          (else
           (adjoin x y xs* ys*))))))))

(define (fill-in-months ls zero)
  (define (next-month date)
    (let ((month (date-month date))
          (year (date-year date)))
      (if (= month 12)
          (make-date 0 0 0 0 1 1 (1+ year) 0)
          (make-date 0 0 0 0 1 (1+ month) year 0))))
  (let lp ((ls ls))
    (match ls
      (() ls)
      ((_) ls)
      (((date . val) . (and ls* ((date* . _) . _)))
       (let ((next (next-month date)))
         (if (equal? next date*)
             ls
             (acons date val
                    (lp (acons next (zero) ls*)))))))))

(define (count-projects-by-month releases)
  (define (truncate-to-month date)
    (make-date 0 0 0 0 1 (date-month date) (date-year date) 0))
  (fill-in-months
   (group-by releases
             (keyed-sorter cdr date-before?)
             (compose truncate-to-month cdr)
             (lambda () 0)
             (lambda (_ sum)
               (1+ sum)))
   (lambda () 0)))

(match (command-line)
  ((_ releases.txt)
   (call-with-values (lambda () (release-ranges releases.txt))
     (lambda (starts ends)
       (for-each
        (lambda (line)
          (match line
            (#(date starts ends)
             (format #t "~a,~a,~a\n" (date->string date "~Y-~m")
                     starts ends))))
        (merge-sorted-lists
         (count-projects-by-month starts)
         (count-projects-by-month ends)
         date-before?
         car
         (lambda () #f)
         (lambda (starts ends)
           (match (vector starts ends)
             (#((date . starts) #f)
              (vector date starts 0))
             (#(#f (date . ends))
              (vector date 0 ends))
             (#((date . starts) (date . ends))
              (vector date starts ends)))))))))
  (_
   (format (current-error-port)
           "Usage: guile terminal-releases-by-month.scm releases.txt\n")
   (exit 1)))
